var mysql = require('mysql');
var MongoClient = require('mongodb').MongoClient;


//Pre-Migration Checks

var con = mysql.createConnection({
  host: "localhost",
  user: "yourusername",
  password: "yourpassword"
});

var preMigrationCheck = mysqlcheck;

mysqlcheck();

async function mysqlcheck() {

await con.connect(function(err) {
  if (err) {
  console.log("Connection to MySQL cannot be established");
  process.exit(1);
  });

var rowVal = [];
 con.query("SELECT top 1 table_name FROM INFORMATION_SCHEMA.TABLES
  WHERE table_schema = 'db_name', function (err, result, fields) {
    if (err) throw err;
    rowVal = result;
  });

var columnDetails = [];
var keys = Object.keys(rowVal);
 for(let i=0;i < rowVal.length;i ++) {
  columnDetails[i] = rowVal[keys[i]]; 
 }

  mongocheck(columnDetails,rowVal);

});

async function mongocheck (columnDetails, rowVal) {
	
	await MongoClient.connect("mongodb://localhost:1234/mongodb", function(err, db) { 
  if(!err) {
    console.log("We are connected");
  }
  else {
  var objectForInsertion = [];
  for(let x =0 ; x< columnDetails.length; x++) {
  objectForInsertion[x] = {columnDetails[x]: rowVal.columnDetails[x]};
  }
   
  dbo.collection("test").insertOne(objectForInsertion, function(err, res) {
    if (err) throw err;
    console.log("Pre migration check Complete");
    db.close();
  });

  }
});
}


// Schema Transformation

schemaTransform();

async function schemaTransform() {

var tableNames = [];
await con.query("SELECT * table_name FROM INFORMATION_SCHEMA.TABLES
  WHERE table_schema = 'db_name'", function (err, result, fields) {
    if (err) throw err;
    tableNames = result;
  });	

var tableNames = [];
var tablesWithConstrain = []; 
var tablesWithoutConstrain = []; // This can be futher refered as collection names

for(let i = 0; i< tableNames.length; i++) {
await con.query("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_SCHEMA = '<database>' AND
  REFERENCED_TABLE_NAME = "+tableNames[i]+"; ", function (err, result, fields) {
    if (err) throw err;
    if(result!= undefined) {  // Moving the tables with no constrain to array
    tablesWithoutConstrain.push(tableNames[i]);
    };
    else {
     tablesWithConstrain.push(results);  // Collecting tables with constrain, Each index holds the group of tables
    } 
  });
}

// Creating collections in MongoDB for tablesWithoutConstrain
for(let i = 0; i< tablesWithoutConstrain.length; i++) {
var columns = [];
// get all columns from mysql 
await con.query("select * 
  from information_schema.columns 
 where table_schema = 'your_DB_name' 
   and table_name = "+tablesWithoutConstrain[i], function (err, result, fields) {
    if (err) throw err;
    columns = result;
  });	

var myobj = {};

for (let trait in columns){ Object.keys(myobj) = trait}

dbo.collection(tablesWithoutConstrain[i]).insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
}

// Creating collections in MongoDB for tablesWithConstrain
 The logic remains similar to same but the nested creation as the table with constraints 
 are fit in the parent collection only
}

dataTransfer();

function dataTransfer() {
	// The array of collections is stored and read operation is first performed here
	//After the completion the MongoDB is pressed into write operation
	
}

}





